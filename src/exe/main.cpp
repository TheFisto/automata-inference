#include <iostream>
#include <random>
#include <algorithm>
#include <string>
#include <fstream>
#include <chrono>
#include <ctime>

#include <base/ILS.hpp>
#include <base/sample.h>
#include <base/solution.h>
#include "../base/HillClimber.h"

#include <eval/smartEval.h>
#include <eval/basicEval.h>
#include <eval/smartBiobjEval.h>

static std::string syntaxe = "Syntaxe :\n ./main train <FILE> <NBSTATES> <SEED> <NBEVAL> <METHOD>\n ./main test <FILE>";
typedef std::pair<double, unsigned> Fitness2;

int main(int argc, char** argv){
    if(argc < 2){
        std::cerr<<syntaxe<<std::endl;
        return 2;
    }
    std::string option = argv[1];

    if(option=="train"){
        if(argc != 7){
            std::cerr<<"Syntaxe :\n ./main train <FILE> <NBSTATES> <SEED> <NBEVAL> <METHOD>"<<std::endl;
            return 1;
        }
        std::string methodOption = argv[6];
        if(methodOption != "ILS" && methodOption != "HC"){
            std::cerr<<"Methods list:\n\t> HC\n\t> ILS"<<std::endl;
            return 1;
        }

        Sample sample(argv[2]);
        std::mt19937 rand(std::stoi(argv[4]));
        SmartEval eval(rand, sample);

        Solution<double> sol( std::stoi(argv[3]), 2);
/*
        std::chrono::time_point<std::chrono::system_clock> start, end;
        start = std::chrono::high_resolution_clock::now();
*/
        if(methodOption == "HC") {
            HillClimber hillC(std::stoi(argv[5]), eval, rand);
            hillC.run(sol);
        }
        else if(methodOption=="ILS"){
            ILS ils(std::stoi(argv[5]), eval, rand);
            ils.run(sol);
        }
/*
        end = std::chrono::high_resolution_clock::now();
        auto elapsed_seconds = end-start;
        std::cout<<argv[5]<<' '<<sol.fitness()<<' '<<(float)elapsed_seconds.count()/1000000000<<std::endl;
*/

        std::cout<<sol.fitness()<<std::endl;

        std::ofstream ofs("solution.out");
        ofs << sol;
        ofs.close();
    }
    else if(option=="test"){
        if(argc != 3){
            std::cerr<<"Syntaxe :\n./main test <FILE>"<<std::endl;
            return 1;
        }
        Sample sample(argv[2]);

        std::ifstream ifs("solution.out");
        Solution<double> xprime;
        ifs >> xprime;
        ifs.close();

        std::mt19937 gen( 1 );

        SmartEval seval(gen, sample);

        seval(xprime);

        std::cout << xprime.fitness() << std::endl << std::endl;


    }
    else{
        std::cerr<<syntaxe<<std::endl;
        return 1;
    }

    return 0;
}

#ifndef LOCALSEARCH_HPP
#define LOCALSEARCH_HPP

#include "../base/solution.h"

class LocalSearch
{
public:
    LocalSearch() {

    }
    virtual void run(Solution<double>& s)=0;
};

#endif // LOCALSEARCH_HPP

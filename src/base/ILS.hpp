//
// Created by ghumetz on 13/12/2019.
//

#ifndef ILS_H
#define ILS_H

#include "localSearch.hpp"
#include <random>
#include "../eval/eval.h"

class ILS : public LocalSearch{
public :
    ILS(int nbEval, Eval<double>& evalFunc, std::mt19937 rand) : LocalSearch(), _nbEval(nbEval), _evalFunc(evalFunc), _random(rand){}

    void run(Solution<double> &s){
        Solution<double> bestSol(s), bestSolGlobal(bestSol);
        bestSol.randomize(_random);
        std::vector<Solution<double>> neighbors = bestSol.getNeighbors();
        double lastFitness = -1;
        _evalFunc(bestSol);
        int cpt = 1;
        while (bestSol.fitness()!=1 && cpt< _nbEval) {
            if(bestSol.fitness() == lastFitness){
                bestSol.randomize(_random);
                _evalFunc(bestSol);
                cpt++;
            }
            lastFitness = bestSol.fitness();
            neighbors = bestSol.getNeighbors();
            for(Solution<double> neighbor: neighbors){
                _evalFunc(neighbor);
                cpt++;
                if(bestSol.fitness()<neighbor.fitness()){
                    bestSol=neighbor;
                }
                if(cpt>=_nbEval){
                    break;
                }
            }
            if(bestSol.fitness()>bestSolGlobal.fitness()){
                bestSolGlobal = bestSol;
            }
        }
        s=bestSolGlobal;
    }

private:
    int _nbEval;
    Eval<double>& _evalFunc;
    std::mt19937 _random;
};

#endif

#ifndef _HILLCLIMBER_H
#define _HILLCLIMBER_H

#include "localSearch.hpp"
#include <random>
#include "../eval/eval.h"

class HillClimber : public LocalSearch{
public :
    HillClimber(int nbEval, Eval<double>& evalFunc, std::mt19937 rand) : LocalSearch(), _nbEval(nbEval), _evalFunc(evalFunc), _random(rand){}

    void run(Solution<double> &s){
        Solution<double> bestSol(s);
        bestSol.randomize(_random);
        std::vector<Solution<double>> neighbors = bestSol.getNeighbors();
        double lastFitness = -1;
        _evalFunc(bestSol);
        int cpt = 1;
        while (bestSol.fitness()!=1 && cpt< _nbEval && lastFitness!=bestSol.fitness()) {
            lastFitness = bestSol.fitness();
            neighbors = bestSol.getNeighbors();
            #pragma omp parallel
            for(Solution<double> neighbor: neighbors){
                _evalFunc(neighbor);
                cpt++;
                if(bestSol.fitness()<neighbor.fitness()){
                    bestSol=neighbor;
                }
                if(cpt>=_nbEval){
                    break;
                }
            }
        }
        s=bestSol;
    }

private:
    int _nbEval;
    Eval<double>& _evalFunc;
    std::mt19937 _random;
};


#endif

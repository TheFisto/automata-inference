#!/bin/bash

echo "algo n rate id type seed fitness" > ".temPerformance.csv"

for methods in ILS ; do
	(for nStates in 32 ; do
		(for rate in 0.1 0.05 0.01; do
			(for seed in {12..29}; do
				(for id in {0..30}; do
					echo $methods $nStates $rate $id train $seed $(./main train ../instances/dfa_${nStates}_${seed}_${rate}_train-sample.json $nStates $id 1000000 $methods) >> ".temPerformance.csv"
					echo $methods $nStates $rate $id test $seed $(./main test "../instances/dfa_${nStates}_${seed}_${rate}_test-sample.json") >>  ".temPerformance.csv"
				done)
			done)
		done)
	done)
done

$(mv .temPerformance.csv performance.csv)

